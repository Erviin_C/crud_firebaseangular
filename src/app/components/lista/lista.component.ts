import { Component, OnInit } from '@angular/core';
import { ConeccionService } from 'src/app/service/coneccion.service';


@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.css']
})
export class ListaComponent implements OnInit {

    items:any;
    editaritem:any = {name: ''}

  constructor(private coneccion: ConeccionService) {
    this.items = this.coneccion.listaItem().subscribe(item=>{this.items=item;
    console.log(this.items)})
   }

  ngOnInit() {
  }

  eliminar(item){
    this.coneccion.eliminarItem(item);
  }

  editar(item){
    this.editaritem = item;
  }

  agregarItemEditado(){
    this.coneccion.editarItem(this.editaritem);
  }

}
