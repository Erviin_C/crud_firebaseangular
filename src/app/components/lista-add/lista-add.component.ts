import { Component, OnInit } from '@angular/core';
import { ConeccionService } from 'src/app/service/coneccion.service';

@Component({
  selector: 'app-lista-add',
  templateUrl: './lista-add.component.html',
  styleUrls: ['./lista-add.component.css']
})
export class ListaAddComponent implements OnInit {

  item:any = {
    name:''
  }
  constructor( private servicio: ConeccionService) { }

  ngOnInit() {
  }

  agregar(){
    this.servicio.agregarItem(this.item);
    this.item.name = '';
  }

}
